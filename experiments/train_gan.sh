#!/bin/sh
cd ..
time python3 gan_net.py train --lambda_identity 0 --dataroot /dataset --name adaptation --model geometrical --pool_size 50 --no_dropout --dataset_mode geometrical --view1 14 --view2 19 --episode 17 --gpu_ids 1 --save_epoch_freq 1 --niter 1 --niter_decay 0
cd ./experiments
