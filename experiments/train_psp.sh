#!/bin/sh
cd ..
time python3 psp_net.py train --dataroot /dataset --views 19 --episodes 17 --max_iter 375 --train_batch_size 8 --val_freq 375 --exp_name baseline --gpu 0 
cd ./experiments
