#!/bin/sh
cd ..
time python3 psp_net.py train --dataroot /dataset --views 14 --episodes 17 --max_iter 1500 --train_batch_size 2 --multi --val_freq 1500 --exp_name upper --gpu 2
cd ./experiments
