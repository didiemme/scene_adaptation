#!/bin/sh
cd ..
python3 psp_net.py test --dataroot /dataset --views 14 --episodes 17 --exp_name baseline --snapshot epoch_3_iter_375_loss_0.20349_acc_0.90968_acc-cls_0.69708_mean-iu_0.64351_fwavacc_0.84160_lr_0.0025656187.pth --multi
cd ./experiments
