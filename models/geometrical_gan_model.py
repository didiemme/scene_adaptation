import sys
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

import itertools
from collections import OrderedDict

import torch
from torch.autograd import Variable

import utils.util as util
from .psp_net import PSPNet
from utils.image_pool import ImagePool

from . import networks
from .base_model import BaseModel

# from .stn import STN

from utils.misc import CrossEntropyLoss2d


class GeometricalGANModel(BaseModel):

    def name(self):
        return 'GeometricalGANModel'

    # FIXED
    def restore(self, which_epoch):
        self.load_network(self.netSemanticGenerator,
                          'SemanticGenerator', which_epoch)
        self.load_network(self.netPhotoGenerator,
                          'PhotoGenerator', which_epoch)
        if self.isTrain:
            self.load_network(self.netDomainDiscriminator,
                              'DomainDiscriminator', which_epoch)

    # FIXED
    def print_info(self):
        print('---------- Networks initialized -------------')
        networks.print_network(self.netSemanticGenerator)
        networks.print_network(self.netPhotoGenerator)
        if self.isTrain:
            networks.print_network(self.netDomainDiscriminator)

        print('-----------------------------------------------')

    # FIXED
    def init_optimizers(self, opt):
        self.pool = ImagePool(opt.pool_size)
        # define loss functions
        self.criterionGAN = networks.MultiGANLoss(
            use_lsgan=not opt.no_lsgan, tensor=self.Tensor)
        self.criterionCycle = torch.nn.L1Loss()
        self.criterionIdt = torch.nn.L1Loss()

        self.criterion = CrossEntropyLoss2d(
            size_average=True, ignore_index=255).cuda()

        # initialize optimizers
        self.optimizer_G = torch.optim.Adam(itertools.chain(self.netSemanticGenerator.parameters(), self.netPhotoGenerator.parameters()),
                                            lr=opt.lr, betas=(opt.beta1, 0.999))
        self.optimizer_D = torch.optim.Adam(
            self.netDomainDiscriminator.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))

        self.optimizers = []
        self.schedulers = []
        self.optimizers.append(self.optimizer_G)
        self.optimizers.append(self.optimizer_D)

        for optimizer in self.optimizers:
            self.schedulers.append(networks.get_scheduler(optimizer, opt))

    # FIXED
    def initialize(self, opt):
        BaseModel.initialize(self, opt)
        # Foto -> Semantic
        self.netSemanticGenerator = PSPNet(num_classes=13, batch=False)
        self.netSemanticGenerator.cuda()
        # Semantic -> Foto
        self.netPhotoGenerator = networks.define_G(13, 3,  # opt.output_nc, opt.input_nc,
                                                   opt.ngf, opt.which_model_netG, opt.norm, not opt.no_dropout, opt.init_type, self.gpu_ids)
        # Domain discriminator
        if self.isTrain:
            use_sigmoid = opt.no_lsgan
            self.netDomainDiscriminator = networks.define_D(opt.output_nc,
                                                            opt.ndf,
                                                            "domain",
                                                            opt.n_layers_D, opt.norm, use_sigmoid, opt.init_type, self.gpu_ids)
        # Restore
        if not self.isTrain or opt.continue_train:
            which_epoch = opt.which_epoch
            self.restore(which_epoch)

        if self.isTrain:
            self.init_optimizers(opt)

        self.print_info()

    # FIXED
    def set_input(self, inputs):

        input_A_Photo = inputs['A_Photo']
        input_A_Semantic = inputs['A_Semantic']
        input_B_Photo = inputs['B_Photo']

        if len(self.gpu_ids) > 0:
            input_A_Photo = input_A_Photo.cuda(self.gpu_ids[0], async=True)
            input_A_Semantic = input_A_Semantic.cuda(self.gpu_ids[0], async=True)
            input_B_Photo = input_B_Photo.cuda(self.gpu_ids[0], async=True)

        self.input_A_Photo = input_A_Photo
        self.input_A_Semantic = input_A_Semantic
        self.input_B_Photo = input_B_Photo

        self.image_paths = inputs['path']  # the idea is that image are in the same path

    # FIXED
    def forward(self):
        self.domain_A_Photo = Variable(self.input_A_Photo)
        self.domain_A_Semantic = Variable(self.input_A_Semantic)
        self.domain_B_Photo = Variable(self.input_B_Photo)

    # FIXED
    def test(self):
        domain_A_Photo = Variable(self.input_A_Photo, volatile=True)
        domain_B_Photo = Variable(self.input_B_Photo, volatile=True)
        semantic_Inference_A = self.netSemanticGenerator(domain_A_Photo)
        semantic_Inference_B = self.netSemanticGenerator(domain_B_Photo)
        self.rec_A_Photo = self.netPhotoGenerator(semantic_Inference_A).data
        self.rec_B_Photo = self.netPhotoGenerator(semantic_Inference_B).data
        self.semantic_Inference_A = semantic_Inference_A.data
        self.semantic_Inference_B = semantic_Inference_B.data

    # FIXED
    def get_image_paths(self):
        return self.image_paths

    # FIXED
    def backward_D_basic(self, netD, real, fake, domain):
        # Real
        pred_real = netD(real)
        loss_D_real = self.criterionGAN(pred_real, domain)
        # Fake
        pred_fake = netD(fake.detach())
        loss_D_fake = self.criterionGAN(pred_fake, 0.0)
        # Combined loss
        loss_D = (loss_D_real + loss_D_fake) * 0.5
        # backward
        loss_D.backward()
        return loss_D

    # FIXED
    def backward_D(self):
        rec_Photo_A = self.pool.query(self.rec_A_Photo)
        rec_Photo_B = self.pool.query(self.rec_B_Photo)

        loss_D_A = self.backward_D_basic(
            self.netDomainDiscriminator, self.domain_A_Photo, rec_Photo_A, 1.0)
        loss_D_B = self.backward_D_basic(
            self.netDomainDiscriminator, self.domain_B_Photo, rec_Photo_B, 2.0)

        self.loss_DomainDiscriminator = loss_D_A.data[0] + loss_D_B.data[0]

    def make_one_hot(self, labels, C=13):
        '''
        Converts an integer label torch.autograd.Variable to a one-hot Variable.

        Parameters
        ----------
        labels : torch.autograd.Variable of torch.cuda.LongTensor
            N x 1 x H x W, where N is batch size.
            Each value is an integer representing correct classification.
        C : integer.
            number of classes in labels.

        Returns
        -------
        target : torch.autograd.Variable of torch.cuda.FloatTensor
            N x C x H x W, where C is class number. One-hot encoded.
        '''
        print (labels.size())
        one_hot = torch.cuda.FloatTensor(labels.size(0), C, labels.size(2), labels.size(3)).zero_()
        print (one_hot.size())
        target = one_hot.scatter_(1, labels.data, 1)

        target = Variable(target)

        return target

    def CELoss2d(self, gts_slice, outputs, aux):

        assert outputs.size()[2:] == gts_slice.size()[1:]
        assert outputs.size()[1] == 13

        main_loss = self.criterion(outputs, gts_slice)
        aux_loss = self.criterion(aux, gts_slice)
        loss = main_loss + 0.4 * aux_loss

        return loss

    # FIXED
    def backward_G(self):
        lambda_idt = self.opt.lambda_identity
        lambda_A = self.opt.lambda_A
        lambda_B = self.opt.lambda_B

        no_reconstruction = self.opt.no_reconstruction
        # Identity loss
        if lambda_idt > 0:

            # G_A should be identity if domain_A_Semantic is fed.
            idt_A_Semantic, _ = self.netSemanticGenerator(self.domain_A_Semantic)
            loss_idt_Semantic = self.criterionIdt(idt_A_Semantic, self.domain_A_Semantic) * lambda_A * lambda_idt

            self.idt_A_Semantic = idt_A_Semantic.data
            self.loss_idt_Semantic = loss_idt_Semantic.data[0]
        else:
            loss_idt_Semantic = 0
            self.loss_idt_Semantic = 0

        # GAN loss D_A(G_A(A))
        """
        pred_fake = self.netDomainDiscriminator()
        loss_SemanticGenerator_A = self.criterionGAN(pred_fake, True)
        loss_SemanticGenerator_B = self.criterionGAN(pred_fake, True)
        """
        semantic_Inference_A, aux_A = self.netSemanticGenerator(self.domain_A_Photo)
        semantic_Inference_B, _ = self.netSemanticGenerator(self.domain_B_Photo)

        print("Semantic Inference", semantic_Inference_A.size())
        print("Semantic Inference Aux", aux_A.size())
        print("domain_A_Semantic", self.domain_A_Semantic.size())

        loss_SemanticGenerator_A = self.CELoss2d(self.domain_A_Semantic[0, :, :, :], semantic_Inference_A, aux_A)
        # loss_SemanticGenerator_A = self.criterionIdt(semantic_Inference_A, self.make_one_hot(self.domain_A_Semantic))
        # Forward cycle loss
        rec_A_Photo = self.netPhotoGenerator(semantic_Inference_A)
        loss_Photo_Rec_Cycle = self.criterionCycle(rec_A_Photo, self.domain_A_Photo)

        rec_B_Photo = self.netPhotoGenerator(semantic_Inference_B)
        loss_Photo_Rec_Cycle += self.criterionCycle(rec_B_Photo, self.domain_B_Photo)  # * lambda_B
        """
        rec_A_FromLabelPhoto = self.netPhotoGenerator(self.domain_A_Semantic)
        loss_Photo_Rec_Cycle += self.criterionCycle(rec_B_Photo, self.domain_A_Photo) * 0.5
        """
        # combined loss
        if not no_reconstruction:
            loss_G = loss_SemanticGenerator_A + loss_Photo_Rec_Cycle + loss_idt_Semantic
        else:
            loss_G = loss_SemanticGenerator_A + loss_idt_Semantic

        loss_G.backward()

        self.semantic_Inference_A = semantic_Inference_A.data
        self.semantic_Inference_B = semantic_Inference_B.data
        self.rec_A_Photo = rec_A_Photo.data
        self.rec_B_Photo = rec_B_Photo.data

        self.loss_SemanticGenerator = loss_SemanticGenerator_A.data[0]
        # self.loss_PhotoGenerator = loss_PhotoGenerator.data[0]
        self.loss_Photo_Rec_Cycle = loss_Photo_Rec_Cycle.data[0]

    # FIXED
    def optimize_parameters(self):
        # forward
        self.forward()
        # G_A and G_B
        self.optimizer_G.zero_grad()
        self.backward_G()
        self.optimizer_G.step()
        # D_A
        if not self.opt.no_adversarial:
            self.optimizer_D.zero_grad()
            self.backward_D()
            self.optimizer_D.step()


    # FIXED
    def get_current_errors(self):
        if not self.opt.no_adversarial:
            ret_errors = OrderedDict([('DomainDiscriminator', self.loss_DomainDiscriminator),
                                      ('SemanticGenerator', self.loss_SemanticGenerator),
                                      ('Photo_Rec_Cycle', self.loss_Photo_Rec_Cycle)])
            #                         ('PhotoGenerator', self.loss_PhotoGenerator)])
        else:
            ret_errors = OrderedDict([('SemanticGenerator', self.loss_SemanticGenerator),
                                      ('Photo_Rec_Cycle', self.loss_Photo_Rec_Cycle)])
            #                         ('PhotoGenerator', self.loss_PhotoGenerator)])
        if self.opt.lambda_identity > 0.0:
            ret_errors['idt_Semantic'] = self.loss_idt_Semantic
        return ret_errors

    # FIXED
    def get_current_visuals(self):
        domain_A_Photo = util.tensor2im(self.input_A_Photo)
        domain_A_Semantic = util.tensor2semantic_palette(self.input_A_Semantic)
        domain_B_Photo = util.tensor2im(self.input_B_Photo)

        semantic_Inference_A = util.tensor2semantic_palette(self.semantic_Inference_A)
        semantic_Inference_B = util.tensor2semantic_palette(self.semantic_Inference_B)

        rec_A_Photo = util.tensor2im(self.rec_A_Photo)
        rec_B_Photo = util.tensor2im(self.rec_B_Photo)

        ret_visuals = OrderedDict([('domain_A_Photo', domain_A_Photo),
                                   ('domain_A_Semantic', domain_A_Semantic),
                                   ('domain_B_Photo', domain_B_Photo),
                                   ('semantic_Inference_A', semantic_Inference_A),
                                   ('rec_A_Photo', rec_A_Photo),
                                   ('semantic_Inference_B', semantic_Inference_B),
                                   ('rec_B_Photo', rec_B_Photo)])

        if self.opt.isTrain and self.opt.lambda_identity > 0.0:
            ret_visuals['idt_Semantic'] = util.tensor2im(self.idt_B)
        return ret_visuals

    # FIXED
    def save(self, label):
        self.save_network(self.netSemanticGenerator,
                          'SemanticGenerator', label, self.gpu_ids)
        self.save_network(self.netDomainDiscriminator,
                          'DomainDiscriminator', label, self.gpu_ids)
        self.save_network(self.netPhotoGenerator,
                          'PhotoGenerator', label, self.gpu_ids)
