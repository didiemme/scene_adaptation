#!/usr/bin/env python3

# Copyright (c) 2017 Computer Vision Center (CVC) at the Universitat Autonoma de
# Barcelona (UAB), and the INTEL Visual Computing Lab.
#
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.

"""Basic CARLA client example."""

from __future__ import print_function

import argparse
import logging
import random
import sys
import time

from carla.client import make_carla_client
from carla.sensor import Camera
from carla.settings import CarlaSettings
from carla.tcp import TCPConnectionError
from carla.util import print_over_same_line


def create_views(views):
    names = []
    angles = []  # [(-30, 0, 15), (-45, 0, 0), (-15, 0, 15)]
    location = []  # [(900, -300, 600), (800, -200, 500), (700, -100, 400)]
    start_x = 700
    start_y = -400
    start_z = 400
    step = 20

    start_pitch = -45
    start_roll = 0
    start_yaw = 0
    step_angle = 2

    for i in views:
        names.append("camera_{}".format(i + 1))
        location.append((start_x + step * i, start_y +
                         step * i, start_z + step * i))
        angles.append((start_pitch + step_angle * i,
                       start_roll, start_yaw + step_angle * i))
    return names, angles, location


def create_cameras(camera_names, angles, location, depth):
    # Now we want to add a couple of cameras to the player vehicle.
    # We will collect the images produced by these cameras every
    # frame.
    image_size = (800, 600)
    cameras = []

    for i, name in enumerate(camera_names):

        camera_car = Camera(name)
        camera_car.set_image_size(*image_size)
        camera_car.set_position(*location[i])
        camera_car.set_rotation(*angles[i])
        cameras.append(camera_car)

        camera_segmentation = Camera(
            name + 'Segmentation', PostProcessing='SemanticSegmentation')
        camera_segmentation.set_image_size(*image_size)
        camera_segmentation.set_position(*location[i])
        camera_segmentation.set_rotation(*angles[i])
        cameras.append(camera_segmentation)

        if depth:
            camera_segmentation = Camera(
                name + 'Depth', PostProcessing='Depth')
            camera_segmentation.set_image_size(*image_size)
            camera_segmentation.set_position(*location[i])
            camera_segmentation.set_rotation(*angles[i])
            cameras.append(camera_segmentation)

    return cameras


def create_settings(cameras):

    settings = CarlaSettings()
    settings.set(
        SynchronousMode=True,
        SendNonPlayerAgentsInfo=True,
        NumberOfVehicles=150,
        NumberOfPedestrians=40,
        WeatherId=random.choice([1, 3, 7, 8, 14]))
    settings.randomize_seeds()

    for camera in cameras:
        settings.add_sensor(camera)

    return settings


def run_carla_client(host,
                     port,
                     autopilot_on,
                     save_images_to_disk,
                     image_filename_format,
                     positions,
                     views,
                     depth,
                     frames_per_episode=5000,
                     settings_filepath=None):
    # Here we will run 3 episodes with 300 frames each.

    fe = frames_per_episode
    if depth:
        fe = 10


    with make_carla_client(host, port) as client:

        for player_start in positions:
            # Notify the server that we want to start the episode at the
            # player_start index. This function blocks until the server is ready
            # to start the episode.
            names, angles, location = create_views(views)
            cameras = create_cameras(names, angles, location, depth)


            # Iterate every frame in the episode.
            for frame in range(0, fe):
                # reset every 1000
                if frame % 1000 == 0:
                    scene = client.load_settings(create_settings(cameras))
                    client.start_episode(player_start)

                # Read the data produced by the server this frame.
                measurements, sensor_data = client.read_data()

                # Print some of the measurements.
                print_measurements(measurements)

                # Save the images to disk if requested.
                if save_images_to_disk:
                    for name, image in sensor_data.items():
                        image.save_to_disk(image_filename_format.format(
                            player_start, name, frame))

                if not autopilot_on:

                    client.send_control(
                        steer=0,
                        throttle=0,
                        brake=True,
                        hand_brake=True,
                        reverse=False)

                else:

                    # Together with the measurements, the server has sent the
                    # control that the in-game autopilot would do this frame. We
                    # can enable autopilot by sending back this control to the
                    # server. We can modify it if wanted, here for instance we
                    # will add some noise to the steer.

                    control = measurements.player_measurements.autopilot_control
                    control.steer += random.uniform(-0.1, 0.1)
                    client.send_control(control)


def print_measurements(measurements):
    number_of_agents = len(measurements.non_player_agents)
    player_measurements = measurements.player_measurements
    message = 'Vehicle at ({pos_x:.1f}, {pos_y:.1f}), '
    message += '{speed:.2f} km/h, '
    message += 'Collision: {{vehicles={col_cars:.0f}, pedestrians={col_ped:.0f}, other={col_other:.0f}}}, '
    message += '{other_lane:.0f}% other lane, {offroad:.0f}% off-road, '
    message += '({agents_num:d} non-player agents in the scene)'
    message = message.format(
        pos_x=player_measurements.transform.location.x / 100,  # cm -> m
        pos_y=player_measurements.transform.location.y / 100,
        speed=player_measurements.forward_speed,
        col_cars=player_measurements.collision_vehicles,
        col_ped=player_measurements.collision_pedestrians,
        col_other=player_measurements.collision_other,
        other_lane=100 * player_measurements.intersection_otherlane,
        offroad=100 * player_measurements.intersection_offroad,
        agents_num=number_of_agents)
    print_over_same_line(message)


if __name__ == '__main__':

    try:
        argparser = argparse.ArgumentParser(description=__doc__)
        argparser.add_argument(
            '-v', '--verbose',
            action='store_true',
            dest='debug',
            help='print debug information')
        argparser.add_argument(
            '--host',
            metavar='H',
            default='localhost',
            help='IP of the host server (default: localhost)')
        argparser.add_argument(
            '-p', '--port',
            metavar='P',
            default=2000,
            type=int,
            help='TCP port to listen to (default: 2000)')
        argparser.add_argument(
            '-a', '--autopilot',
            action='store_true',
            help='enable autopilot')
        argparser.add_argument(
            '-i', '--images-to-disk',
            action='store_true',
            help='save images to disk')
        argparser.add_argument(
            '-c', '--carla-settings',
            metavar='PATH',
            default=None,
            help='Path to a "CarlaSettings.ini" file')

        argparser.add_argument(
            '-V', '--views',
            nargs='+',
            default=[0, 13, 18],
            help='Path to a "CarlaSettings.ini" file')

        argparser.add_argument(
            '-P', '--positions',
            nargs='+',
            default=[17, 88, 151],
            help='Path to a "CarlaSettings.ini" file')

        argparser.add_argument(
            '-d', '--depth',
            action='store_true',
            help='Path to a "CarlaSettings.ini" file')

        argparser.add_argument(
            '-f', '--frames',
            type=int,
            help='Path to a "CarlaSettings.ini" file')

        argparser.add_argument(
            '-s', '--save_path',
            default="/mnt/repository/ddimauro/images_final",
            type=str,
            help='Path to a "CarlaSettings.ini" file')

        args = argparser.parse_args()

        log_level = logging.DEBUG if args.debug else logging.INFO
        logging.basicConfig(
            format='%(levelname)s: %(message)s', level=log_level)

        logging.info('listening to server %s:%s', args.host, args.port)

        while True:
            try:

                run_carla_client(
                    host=args.host,
                    port=args.port,
                    autopilot_on=args.autopilot,
                    save_images_to_disk=args.images_to_disk,
                    positions=args.positions,
                    views=args.views,
                    frames_per_episode=args.frames,
                    depth=args.depth,
                    image_filename_format=args.save_path + '/episode_{:0>3d}/{:s}/image_{:0>5d}.png',
                    settings_filepath=args.carla_settings)

                print('Done.')
                break

            except TCPConnectionError as error:
                logging.error(error)
                time.sleep(1)
            except Exception as exception:
                logging.exception(exception)
                sys.exit(1)

    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')
