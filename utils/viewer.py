from PIL import Image
import numpy as np

CLASSES = {0: "None",
           1: "Buildings",
           2: "Fences",
           3: "Other",
           4: "Pedestrians",
           5: "Poles",
           6: "RoadLines",
           7: "Roads",
           8: "Sidewalks",
           9: "Vegetation",
           10: "Vehicles",
           11: "Walls",
           12: "TrafficSigns"}

im = np.array(Image.open("/mnt/repository/ddimauro/images_final/episode_017/camera_1Segmentation/image_00026.png"))[:,:,0]

im = Image.fromarray(np.uint8(im/12.0 * 255))
im.show(command='fim')
