def loss(image_a, mask_a, recon_a, image_b, mask_b, recon_b):
    return loss_semantics(image_a, mask_a) + loss_semantics(image_b, mask_b) +\
        loss_reconstruction(image_a, recon_a) + \
        loss_reconstruction(image_b, recon_b) + loss_domain(recon_b)


def loss_semantics(image, mask):
    pass


def loss_reconstruction(image, recon):
    pass


def loss_domain(image):
    pass


def geometric_inverse():
    pass
