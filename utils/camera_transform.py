import math
import numpy as np
# import cv2

# getAffineTransform https://stackoverflow.com/questions/38320865/how-to-replace-a-contour-rectangle-in-an-image-with-a-new-image-using-python/38323528#38323528
# getPerspectiveTransform http://uberhip.com/python/image-processing/opencv/2014/10/26/warping-brien/
# findHomography http://www.learnopencv.com/homography-examples-using-opencv-python-c/#download

# https://it.mathworks.com/matlabcentral/answers/166497-create-a-projective-homography-matrix-with-pitch-roll-yaw?s_tid=gn_loc_drop

"""

K = [[f, 0, Cu],
     [0, f, Cv],
     [0, 0, 1 ]]


"""


class CarlaCamera(object):

    def __init__(self, camera_id):
        self.name = "camera {}".format(camera_id)

        self.start_x = 700
        self.start_y = -400
        self.start_z = 400
        self.step = 20

        self.start_pitch = -45
        self.start_roll = 0
        self.start_yaw = 0
        self.step_angle = 2

        self.location = (self.start_x + self.step * camera_id, self.start_y + self.step * camera_id, self.start_z + self.step * camera_id)
        self.angles = (self.start_pitch + self.step_angle * camera_id, self.start_roll, self.start_yaw + self.step_angle * camera_id)

        self.ImageSizeX = 800
        self.ImageSizeY = 600
        self.CameraFOV = 90

        self.K = None
        self.matrix = None

    def get_K(self):

        Cu = self.ImageSizeX / 2
        Cv = self.ImageSizeY / 2
        f = self.ImageSizeX / (2 * math.tan(self.CameraFOV * math.pi / 360))
        self.K = np.matrix([[f, 0, Cu],
                            [0, f, Cv],
                            [0, 0, 1 ]])

        return self.K

    def get_matrix(self):

        yaw = np.radians(self.angles[2])
        roll = np.radians(self.angles[1])
        pitch = np.radians(self.angles[0])

        yaw_matrix = np.matrix([
            [math.cos(yaw), -math.sin(yaw), 0],
            [math.sin(yaw), math.cos(yaw), 0],
            [0, 0, 1]
        ])

        pitch_matrix = np.matrix([
            [math.cos(pitch), 0, -math.sin(pitch)],
            [0, 1, 0],
            [math.sin(pitch), 0, math.cos(pitch)]
        ])

        roll_matrix = np.matrix([
            [1, 0, 0],
            [0, math.cos(roll), math.sin(roll)],
            [0, -math.sin(roll), math.cos(roll)]
        ])

        rotation_matrix = yaw_matrix * pitch_matrix * roll_matrix

        translation = np.matrix([
            [self.location[0]],
            [self.location[1]],
            [self.location[2]]
        ])

        self.matrix = np.hstack((rotation_matrix, translation))

        return self.matrix

    def __str__(self):
        alpha, beta, gamma = self.angles
        x, y, z = self.location
        obj_string = 'camera {}, x={} y={} z={}, p={} r={} y={}\n'.format(
            self.name, x, y, z, alpha, beta, gamma)

        return obj_string


if __name__ == '__main__':

    cameras = []
    for i in [0, 13, 18]:
        camera = CarlaCamera(i)
        cameras.append(camera)
        
