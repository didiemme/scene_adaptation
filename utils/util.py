from __future__ import print_function
import torch
import numpy as np
from PIL import Image
import os

SHAPE = 512

# Converts a Tensor into a Numpy array
# |imtype|: the desired type of the converted numpy array
def tensor2im(image_tensor, imtype=np.uint8):
    image_numpy = image_tensor[0].cpu().float().numpy()
    if image_numpy.shape[0] == 1:
        image_numpy = np.tile(image_numpy, (3, 1, 1))
    image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0
    return image_numpy.astype(imtype)


def tensor2semantic(image_tensor, imtype=np.uint8, shape=SHAPE):
    image_numpy = image_tensor[0].cpu().float().numpy()
    if image_numpy.shape[0] == 13:
        image_numpy = image_numpy.argmax(0).reshape((1,shape,shape))
    if image_numpy.shape[0] == 1:
        image_numpy = np.tile(image_numpy, (3, 1, 1))
    print (image_numpy.shape)
    image_numpy = (np.transpose(image_numpy, (1, 2, 0)) / 12.0) * 255.0
    return image_numpy.astype(imtype)


def tensor2semantic_palette(image_tensor, imtype=np.uint8, shape=SHAPE):
    image_numpy = image_tensor[0].cpu().float().numpy()
    palette = [(128, 64, 128),
               (244, 35, 232),
               (70, 70, 70),
               (102, 102, 156),
               (190, 153, 153),
               (153, 153, 153),
               (250, 170, 30),
               (220, 220, 0),
               (107, 142, 35),
               (152, 251, 152),
               (70, 130, 180),
               (220, 20, 60),
               (255, 0, 0)]

    # print (image_numpy.max())

    if image_numpy.shape[0] == 13:
        image_numpy = image_numpy.argmax(0).reshape((1,shape,shape))

    image_numpy = np.transpose(image_numpy, (1, 2, 0))

    mask = np.zeros((shape, shape, 3))
    for x in range(shape):
        for y in range(shape):
            mask[x, y, :] = np.array(palette[int(image_numpy[x,y])])

    return mask.reshape((shape,shape,3)).astype(imtype)


def diagnose_network(net, name='network'):
    mean = 0.0
    count = 0
    for param in net.parameters():
        if param.grad is not None:
            mean += torch.mean(torch.abs(param.grad.data))
            count += 1
    if count > 0:
        mean = mean / count
    print(name)
    print(mean)


def save_image(image_numpy, image_path):
    image_pil = Image.fromarray(image_numpy)
    image_pil.save(image_path)


def print_numpy(x, val=True, shp=False):
    x = x.astype(np.float64)
    if shp:
        print('shape,', x.shape)
    if val:
        x = x.flatten()
        print('mean = %3.3f, min = %3.3f, max = %3.3f, median = %3.3f, std=%3.3f' % (
            np.mean(x), np.min(x), np.max(x), np.median(x), np.std(x)))


def mkdirs(paths):
    if isinstance(paths, list) and not isinstance(paths, str):
        for path in paths:
            mkdir(path)
    else:
        mkdir(paths)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)
