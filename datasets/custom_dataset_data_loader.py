import torch.utils.data
from datasets.base_data_loader import BaseDataLoader


def CreateDataset(opt):
    dataset = None
    if opt.dataset_mode == 'aligned':
        from datasets.aligned_dataset import AlignedDataset
        dataset = AlignedDataset()
    elif opt.dataset_mode == 'unaligned':
        from datasets.unaligned_dataset import UnalignedDataset
        dataset = UnalignedDataset()
    elif opt.dataset_mode == 'single':
        from datasets.single_dataset import SingleDataset
        dataset = SingleDataset()
    elif opt.dataset_mode == 'geometrical':
        from datasets.carla import GanCarlaDataset

        if opt.dataset1 is not None:
            _, ep_a, vi_a = (i.split(":")[1] for i in opt.dataset1.split(","))
            _, ep_b, vi_b = (i.split(":")[1] for i in opt.dataset2.split(","))

        else:
            ep_a = opt.episode
            ep_b = opt.episode
            vi_a = opt.view1
            vi_b = opt.view2

        split = "train" if opt.isTrain else "test"
        dataset = GanCarlaDataset(split,
                                  episode_a=int(ep_a),
                                  episode_b=int(ep_b),
                                  view_a=int(vi_a),
                                  view_b=int(vi_b),
                                  root=opt.dataroot)
    else:
        raise ValueError("Dataset [%s] not recognized." % opt.dataset_mode)

    print("dataset [%s] was created" % (dataset.name()))
    dataset.initialize(opt)
    return dataset


class CustomDatasetDataLoader(BaseDataLoader):
    def name(self):
        return 'CustomDatasetDataLoader'

    def initialize(self, opt):
        BaseDataLoader.initialize(self, opt)
        self.dataset = CreateDataset(opt)
        self.dataloader = torch.utils.data.DataLoader(
            self.dataset,
            batch_size=opt.batchSize,
            shuffle=not opt.serial_batches,
            num_workers=int(opt.nThreads))

    def load_data(self):
        return self

    def __len__(self):
        return min(len(self.dataset), self.opt.max_dataset_size)

    def __iter__(self):
        for i, data in enumerate(self.dataloader):
            if i >= self.opt.max_dataset_size:
                break
            yield data
