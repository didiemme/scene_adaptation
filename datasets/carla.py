import glob
import os
import random

import cv2
import numpy as np
import torch
import torch.utils.data as data
import torchvision.transforms as transforms
from PIL import Image

# import datasets.findhomography as fh

from .base_dataset import get_transform

palette = [128, 64, 128,
           244, 35, 232,
           70, 70, 70,
           102, 102, 156,
           190, 153, 153,
           153, 153, 153,
           250, 170, 30,
           220, 220, 0,
           107, 142, 35,
           152, 251, 152,
           70, 130, 180,
           220, 20, 60,
           255, 0, 0]

zero_pad = 256 * 3 - len(palette)

palette += ([0] * zero_pad)


def colorize_mask(mask, pal=palette):
    # mask: numpy array of the mask
    new_mask = Image.fromarray(mask.astype(np.uint8)).convert('P')
    new_mask.putpalette(pal)

    return new_mask


class GeometricalTransform(object):
    # affine type is not used

    def __init__(self, view1, view2, affine_type, rando, episode=17):  # gli episodi sono indifferenti
        self.view1 = int(view1)
        self.view2 = int(view2)
        self.episode = episode
        self.affine_type = affine_type
        self.seed = 2014
        self.r = random.Random(self.seed)
        self.random = rando

    def __call__(self, img, mask):
        M, bbox, min_box = fh.load_mask_boxes(
            self.episode, self.view1, self.view2)

        x, y, w, h = bbox

        imga = np.asarray(img, dtype=np.uint8)

        height = mask.shape[0]
        width = mask.shape[1]

        montecarlo = self.r.random() if self.random else 1

        if montecarlo > 0.5:
            r_img = np.zeros((height * 2, width * 2, 3), np.uint8)
            r_img[:, :, 0] = cv2.warpPerspective(
                self.center(imga[:, :, 0]), M, (width * 2, height * 2))
            r_img[:, :, 1] = cv2.warpPerspective(
                self.center(imga[:, :, 1]), M, (width * 2, height * 2))
            r_img[:, :, 2] = cv2.warpPerspective(
                self.center(imga[:, :, 2]), M, (width * 2, height * 2))
            r_mask = cv2.warpPerspective(
                self.center(mask), M, (width * 2, height * 2))

            x = x + int((w - width) / 2)
            y = y + int((h - height) / 2)

            r_img = r_img[y:y + height, x:x + width, :]
            r_mask = r_mask[y:y + height, x:x + width]
        else:
            r_img = imga
            r_mask = mask

        """
        # print (min_box)
        if self.affine_type == 'crop':
            # print(bbox)
            x, y, w, h = bbox
            if montecarlo > 0.5:
                x = (width - w) / 2
                y = (height - h) / 2
            r_img = r_img[y:y + h, x:x + w, :]
            r_mask = r_mask[y:y + h, x:x + w]
        elif self.affine_type == 'inside':
            x, y, w, h = min_box
            if montecarlo > 0.5:
                x = (width - w) / 2
                y = (height - h) / 2
            r_img = r_img[y:y + h, x:x + w, :]
            r_mask = r_mask[y:y + h, x:x + w]
        """
        return Image.fromarray(r_img.astype('uint8'), 'RGB'), r_mask

    def center(self, imga):

        height = imga.shape[0]
        width = imga.shape[1]
        channels = 1 if len(imga.shape) == 2 else imga.shape[2]

        h = int(height / 2)
        w = int(width / 2)

        img1 = np.zeros((height * 2, width * 2, channels), np.uint8)
        if channels == 1:
            img1 = img1.reshape(img1.shape[0], img1.shape[1])
            img1[h:h + height, w:w + width] = imga
        else:
            img1[h:h + height, w:w + width, :] = imga

        return img1


class GanCarlaDataset(data.TensorDataset):

    def name(self):
        return 'GanCarlaDataset'

    def initialize(self, opt):
        self.opt = opt
        # self.transform = get_transform(opt)
        # self.transform_semantic = get_transform(opt, False)

    def __init__(self,
                 split,
                 episode_a,
                 episode_b,
                 view_a,
                 view_b,
                 classnumbers=13,
                 debug=False,
                 root="./storage"):

        self.split = split
        if split == "train":
            self.start = 0
            self.end = 0.6
        elif split == "test":
            self.start = 0.6
            self.end = 0.8
        elif split == "video":
            self.start = 0.0
            self.end = 1.0
        else:
            self.start = 0.8
            self.end = 1.0
        self.root = os.path.abspath(root)
        self.classnumbers = classnumbers
        self.episode_a = episode_a
        self.episode_b = episode_b
        self.view_a = view_a
        self.view_b = view_b
        self.seed = 2018

        self.images_a = self.__images(self.episode_a, self.view_a)
        self.images_b = self.__images(self.episode_b, self.view_b)
        self.labels_a = self.__labels(self.episode_a, self.view_a)
        self.labels_b = self.__labels(self.episode_b, self.view_b)

    def __images(self, e, v):
        path = os.path.join(self.root, "episode_{:03d}".format(e), "camera_{}".format(v), "*.png")
        files = glob.glob(path)
        files.sort()
        if self.split != "video":
            random.Random(self.seed).shuffle(files)
        tot = len(files)
        return files[int(self.start * tot):int(self.end * tot)]

    def __labels(self, e, v):
        path = os.path.join(self.root, "episode_{:03d}".format(e), "camera_{}Segmentation".format(v), "*.png")
        files = glob.glob(path)
        files.sort()
        if self.split != "video":
            random.Random(self.seed).shuffle(files)
        tot = len(files)
        return files[int(self.start * tot):int(self.end * tot)]

    def __len__(self):
        return len(self.images_a)

    def __getitem__(self, index):

        A_Photo = Image.open(self.images_a[index]).convert('RGB')
        B_Photo = Image.open(self.images_b[index]).convert('RGB')
        A_Semantic = Image.open(self.labels_a[index]).convert('RGB')

        w = 800
        h = 600
        fineSize = 512
        w_offset = random.randint(0, max(0, w - fineSize - 1))
        h_offset = random.randint(0, max(0, h - fineSize - 1))
        box = (w_offset, h_offset, w_offset + fineSize, h_offset + fineSize)
        A_Photo = A_Photo.crop(box)
        B_Photo = B_Photo.crop(box)
        A_Semantic = A_Semantic.crop(box)

        A_Semantic = np.array(A_Semantic)[:, :, 0]  # take only red channel
        # mask = Image.fromarray(mask.astype(np.uint8))  # read as unsigned int
        # A_Semantic = (np.arange(A_Semantic.max()+1) ==
        # A_Semantic[...,None]).astype(int) # One Hot Encoding
        A_Semantic = Image.fromarray(A_Semantic.astype(
            np.uint8), mode="L")  # read as unsigned int

        # me = ((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
        me = ((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        A_Photo = transforms.ToTensor()(A_Photo)
        # A_Photo = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))(A_Photo)
        A_Photo = transforms.Normalize(*me)(A_Photo)

        B_Photo = transforms.ToTensor()(B_Photo)
        # B_Photo = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))(B_Photo)
        B_Photo = transforms.Normalize(*me)(B_Photo)

        A_Semantic = torch.from_numpy(np.array(A_Semantic, dtype=np.int32)).long().view(
            1, fineSize, fineSize)  # transforms.ToTensor()(A_Semantic)

        return {'A_Photo': A_Photo,
                'B_Photo': B_Photo,
                'A_Semantic': A_Semantic,
                'path': self.images_a[index]}


class CarlaDataset(data.TensorDataset):

    CLASSES = {0: "None",
               1: "Buildings",
               2: "Fences",
               3: "Other",
               4: "Pedestrians",
               5: "Poles",
               6: "RoadLines",
               7: "Roads",
               8: "Sidewalks",
               9: "Vegetation",
               10: "Vehicles",
               11: "Walls",
               12: "TrafficSigns"}

    def __init__(self,
                 split,
                 views=[0, 13, 18],
                 episodes=[17, 88, 151],
                 joint_transform=None,
                 sliding_crop=None,
                 transform=None,
                 target_transform=None,
                 geometrical_transform=None,
                 classnumbers=13,
                 debug=False,
                 root="./storage"):
        self.split = split
        if split == "train":
            self.start = 0
            self.end = 0.6
        elif split == "test":
            self.start = 0.6
            self.end = 0.8
        elif split == "video":
            self.start = 0.0
            self.end = 1.0
        else:
            self.start = 0.8
            self.end = 1.0
        self.root = os.path.abspath(root)
        self.classnumbers = classnumbers
        self.episodes = episodes
        self.views = views
        self.joint_transform = joint_transform
        self.sliding_crop = sliding_crop
        self.transform = transform
        self.target_transform = target_transform
        self.geometrical_transform = geometrical_transform
        self.seed = 2018
        self.images = self.__images()
        self.labels = self.__labels()
        self.joint_transform = joint_transform
        self.sliding_crop = sliding_crop
        self.transform = transform
        self.target_transform = target_transform

    def __images(self):

        files = []
        for e in self.episodes:
            for v in self.views:
                path = os.path.join(self.root, "episode_{:03d}".format(
                    e), "camera_{}".format(v), "*.png")
                files += glob.glob(path)
        files.sort()
        if self.split != "video":
            random.Random(self.seed).shuffle(files)
        tot = len(files)
        return files[int(self.start * tot):int(self.end * tot)]

    def __labels(self):
        files = []
        for e in self.episodes:
            for v in self.views:
                path = os.path.join(self.root, "episode_{:03d}".format(
                    e), "camera_{}Segmentation".format(v), "*.png")
                files += glob.glob(path)
        files.sort()
        if self.split != "video":
            random.Random(self.seed).shuffle(files)
        tot = len(files)
        return files[int(self.start * tot):int(self.end * tot)]

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):

        img = Image.open(self.images[index]).convert('RGB')
        mask = Image.open(self.labels[index]).convert('RGB')

        mask = np.array(mask)[:, :, 0]  # take only red channel
        # mask = Image.fromarray(mask.astype(np.uint8))  # read as unsigned int

        if self.geometrical_transform is not None:
            img, mask = self.geometrical_transform(img, mask)

        mask = Image.fromarray(mask.astype(np.uint8))  # read as unsigned int

        if self.joint_transform is not None:
            img, mask = self.joint_transform(img, mask)
        if self.sliding_crop is not None:
            img_slices, mask_slices, slices_info = self.sliding_crop(img, mask)
            if self.transform is not None:
                img_slices = [self.transform(e) for e in img_slices]
            if self.target_transform is not None:
                mask_slices = [self.target_transform(e) for e in mask_slices]
            img, mask = torch.stack(img_slices, 0), torch.stack(mask_slices, 0)
            return img, mask, torch.LongTensor(slices_info)
        else:
            if self.transform is not None:
                img = self.transform(img)
            if self.target_transform is not None:
                mask = self.target_transform(mask)
            return img, mask


if __name__ == '__main__':
    val = CarlaDataset("train", views=[1], episodes=[
                       17], root="/storage/images_final")

    print (val.images)
    print (val.labels)
