import itertools
import pickle

import cv2
import numpy as np
from matplotlib import pyplot as plt

np.set_printoptions(suppress=True)
MIN_MATCH_COUNT = 10
cameras = [1, 14, 19]

# https://stackoverflow.com/questions/21410449/how-do-i-crop-to-largest-interior-bounding-box-in-opencv/21479072#21479072
def autocrop(img, save=False, outname="crop.jpg"):

    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(img, 1, 255, cv2.THRESH_BINARY)

    img2, contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Find with the largest rectangle
    areas = [cv2.contourArea(contour) for contour in contours]
    max_index = np.argmax(areas)
    cnt = contours[max_index]

    x, y, w, h = cv2.boundingRect(cnt)

    crop = img[y:y + h, x:x + w]

    if save:
        cv2.imwrite(outname, crop)

    return x, y, w, h


def find_min_rect(M, warp_src=None, save=False, outname="inside.jpg"):
    upper_left = (M * np.matrix([400, 300, 1]).T)
    lower_left = (M * np.matrix([400, 900, 1]).T)

    upper_right = (M * np.matrix([1200, 300, 1]).T)
    lower_right = (M * np.matrix([1200, 900, 1]).T)

    stack = np.vstack((upper_left.T / upper_left[2, 0], lower_left.T / lower_left[
                      2, 0], upper_right.T / upper_right[2, 0], lower_right.T / lower_right[2, 0]))

    stack = np.hstack((stack[:, 0].clip(min=0, max=1600), stack[
                      :, 1].clip(min=0, max=1200))).astype(np.int16)

    xses = stack[:,0].T
    yses = stack[:,1].T

    xses = np.sort(xses)
    yses = np.sort(yses)

    x, y = xses[0,1], yses[0,1]
    w = xses[0,2] - x
    h = yses[0,2] - y
    print (x, y, w, h)
    if save and (warp_src is not None):
        crop = img[y:y + h, x:x + w]
        cv2.imwrite(outname, crop)

    return x, y, w, h


def matching(img1, img2, width=800, height=600):
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1, des2, k=2)
    M = None
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            good.append(m)

    if len(good) > MIN_MATCH_COUNT:
        src_pts = np.float32(
            [kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32(
            [kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

    else:
        return None, None

    warp_src = cv2.warpPerspective(img1, M, (width * 2, height * 2))

    return M, warp_src


def create_matrix(fname="masks.pkl", width=800, height=600, show=False, save_images=False):

    permutations = {}

    for a, b in itertools.permutations(cameras, 2):
        for e in [17, 88, 151]:
            # queryImage
            imga = cv2.imread(
                '../../depth_images/episode_{:03d}/camera_{}/image_00009.png'.format(e, a), 0)
            # trainImage
            imgb = cv2.imread(
                '../../depth_images/episode_{:03d}/camera_{}/image_00009.png'.format(e, b), 0)

            h = int(height / 2)
            w = int(width / 2)

            img1 = np.zeros((height * 2, width * 2), np.uint8)
            img1[h:h + height, w:w + width] = imga

            img2 = np.zeros((height * 2, width * 2), np.uint8)
            img2[h:h + height, w:w + width] = imgb

            M, warp_src = matching(img1, img2)

            if M is None:
                continue

            if show:
                fig, (a1, b1, c1) = plt.subplots(1, 3)

                a1.imshow(np.maximum(warp_src, img2), 'gray')
                b1.imshow(img1, 'gray')
                c1.imshow(img2, 'gray')

                plt.show()

            if save_images:
                cv2.imwrite("output_{}_{}_{}.jpg".format(e, a, b), warp_src)

            permutations[(e, a, b)] = M
            permutations[(e, a, b, "bbox")] = autocrop(warp_src)
            permutations[(e, a, b, "min_box")] = find_min_rect(M, warp_src)
    save_obj(permutations, fname)
    return permutations

def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


def load_mask_boxes(episode, view_one, view_two, fname="/workspace/source/masks.pkl"):
    try:
        masks = load_obj(fname)
    except:
        create_matrix(fname)
        masks = load_obj(fname)

    return masks[(episode, view_one, view_two)], masks[(episode, view_one, view_two, 'bbox')], masks[(episode, view_one, view_two, 'min_box')]


def test_mask(view_a, view_b, width=800, height=600):
    h = int(height / 2)
    w = int(width / 2)

    img1 = np.zeros((height * 2, width * 2), np.uint8)
    img1[h:h + height, w:w + width] = np.ones((height, width)) * 255

    warped = []
    for e in [17, 88, 151]:
        M = load_mask(e, view_a, view_b)

        warped.append(cv2.warpPerspective(img1, M, (width * 2, height * 2)))

    fig, (a1, b1, c1, d1) = plt.subplots(1, 4)

    a1.imshow(img1, 'gray')
    b1.imshow(warped[0], 'gray')
    c1.imshow(warped[1], 'gray')
    d1.imshow(warped[1], 'gray')

    plt.show()


if __name__ == '__main__':
    p = create_matrix(save_images=True)
    img = cv2.imread("output_{}_{}_{}.jpg".format(151, 14, 19))
    find_min_rect(p[(151, 14, 19)], img, save=True)
    autocrop(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), save=True)
