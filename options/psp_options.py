import argparse
from math import sqrt


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class PspOptions(object):

    def __init__(self):
        self.parser = argparse.ArgumentParser()

    def initialize(self):
        self.parser.add_argument('mode', default='train',
                                 choices=['train', 'test', 'both', 'video'],
                                 help='batch size training')

        self.parser.add_argument('--exp_name', type=str, default='carla-psp_net',
                                 help='batch size training')
        self.parser.add_argument('--episodes', nargs="+", type=int,
                                 help='list of episodes')
        self.parser.add_argument('--views', nargs="+", type=int,
                                 help='list of views')

        self.parser.add_argument('--ckpt_path', type=str, default="ckpt_path",
                                 help='list of views')

        self.parser.add_argument('--train_batch_size', type=int, default=2,
                                 help='batch size training')

        self.parser.add_argument('--test_batch_size', type=int, default=1,
                                 help='batch size training')

        self.parser.add_argument('--lr', type=float, default=1e-2 / sqrt(16 / 2),
                                 help='initial learning_rate')
        self.parser.add_argument('--lr_decay', type=float, default=0.9,
                                 help='learning rate decay')

        self.parser.add_argument('--max_iter', type=float, default=9e4,
                                 help='max number of iterations')

        self.parser.add_argument('--width', type=int, default=800,
                                 help='image longer side')
        self.parser.add_argument('--height', type=int, default=600,
                                 help='image other side')

        self.parser.add_argument('--crop_size', type=int, default=600,
                                 help='cropping size')

        self.parser.add_argument('--stride_rate', type=float, default=2 / 3,
                                 help='stride')
        self.parser.add_argument('--weight_decay', type=float, default=1e-4,
                                 help='weight decay')
        self.parser.add_argument('--momentum', type=float, default=0.9,
                                 help='momentum')

        self.parser.add_argument('--snapshot', type=str, default='',
                                 help='max number of iterations')
        self.parser.add_argument('--print_freq', type=int, default=10,
                                 help='image longer side')
        self.parser.add_argument('--val_save_to_img_file', type=str2bool,
                                 default='False',
                                 help='cropping size')

        self.parser.add_argument('--val_img_sample_rate', type=float, default=0.01,
                                 help='validation rate')
        self.parser.add_argument('--val_img_display_size', type=int, default=384,
                                 help='validation image display')
        self.parser.add_argument('--val_freq', type=int, default=400,
                                 help='validation frequency')

        self.parser.add_argument('--affine', nargs=2,
                                 help='affine transform view')

        self.parser.add_argument('--affine_type', default='all',
                                 choices=['all', 'crop', 'inside'],
                                 help='batch size training')

        self.parser.add_argument('--multi', action='store_true',
                                 help='multi gpu')

        self.parser.add_argument('--ignore', type=int, default=255,
                                 help='label to ignore')

        self.parser.add_argument('--random', action='store_true',
                                 help='label to ignore')

        self.parser.add_argument('--nobatchnorm', action='store_false',
                                 help='label to ignore')

        self.parser.add_argument('--sliding', action='store_false',
                                 help='label to ignore')

        self.parser.add_argument('--mean', default='a',
                                 choices=['a', 'b'],
                                 help='batch size training')

        self.parser.add_argument('--gpu',
                                 type=str,
                                 default='0,1,2,3',
                                 help='batch size training')

        self.parser.add_argument('--postfix',
                                 type=str)

        self.parser.add_argument('--dataroot', required=True, help='path to images')
