# SceneAdapt: Semantic Segmentation Adaptation Through Adversarial Learning

This code was developed for testing work described in:  
Daniele Di Mauro, Antonino Furnari, Giuseppe Patan�, Sebastiano Battiato, Giovanni Maria Farinella,  
SceneAdapt: Scene-based domain adaptation for semantic segmentation using adversarial learning,  
Pattern Recognition Letters, Volume 136, Pages 175-182, 2020  
https://doi.org/10.1016/j.patrec.2020.06.002  


	@article{di2020sceneadapt,
	  title={SceneAdapt: Scene-Based Domain Adaptation for Semantic Segmentation using Adversarial Learning},
	  author={Di Mauro, Daniele and Furnari, Antonino and Patan{\`e}, Giuseppe and Battiato, Sebastiano and Farinella, Giovanni Maria},
	  journal={Pattern Recognition Letters},
	  year={2020},
	  publisher={Elsevier}
	}


The code is based on CycleGan implementation in https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix and PSPnet is
taken from https://github.com/zijundeng/pytorch-semantic-segmentation.

## Where to start

### Experiments
All the scripts are in the /experiments folder:

1. download the dataset http://iplab.dmi.unict.it/SceneAdapt/carla_generated_dataset.tgz
2. download pre-trained models from https://github.com/pytorch/vision/tree/master/torchvision/models and correct models/config.py accordingly.
3. use the train_psp.sh, train_gan.sh, train_upper.sh to train 3 different models.
4. use baseline.sh, upper_bound.sh, scene_adapt.sh to infer results over the
3 models.

### Carla Client
The code in /carla_client was used to create the dataset.

### Relevant code
The model implementation is models/geometrical_gan_model.py and the dataset loader in datasets/carla.py.

