# python3 psp_net.py train --views 14 --episodes 17 --snapshot
# epoch_28_iter_300_loss_0.16828_acc_0.92489_acc-cls_0.82873_mean-iu_0.75808_fwavacc_0.86752_lr_0.0007706080.pth
# --max_iter 50000

import datetime
import os

import numpy as np
import torch
import torchvision.transforms as standard_transforms
import torchvision.utils as vutils
from torch import optim
from torch.autograd import Variable
from torch.utils.data import DataLoader

import utils.joint_transforms as joint_transforms
import utils.transforms as extended_transforms
from datasets import carla
import datasets.findhomography as fh
from models import psp_net
from options.psp_options import PspOptions
from tensorboardX import SummaryWriter
from utils import AverageMeter, CrossEntropyLoss2d, check_mkdir, evaluate

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1,2,3"


def train(args):

    episodes = args["episodes"]
    views = args["views"]
    exp_name = args["exp_name"]
    ckpt_path = args["ckpt_path"]

    ignore_label = args['ignore']
    writer = SummaryWriter(os.path.join(ckpt_path, 'exp', exp_name))

    net = psp_net.PSPNet(num_classes=len(carla.CarlaDataset.CLASSES.keys()), batch=args["nobatchnorm"])
    if args["multi"]:
        net = torch.nn.DataParallel(net)

    if len(args['snapshot']) == 0:

        curr_epoch = 1
        args['best_record'] = {'epoch': 0,
                               'iter': 0,
                               'val_loss': 1e10,
                               'acc': 0,
                               'acc_cls': 0,
                               'mean_iu': 0,
                               'fwavacc': 0}
    else:
        print('training resumes from ' + args['snapshot'])
        net.load_state_dict(torch.load(os.path.join(
            ckpt_path, exp_name, args['snapshot'])))
        split_snapshot = args['snapshot'].split('_')
        curr_epoch = int(split_snapshot[1]) + 1
        args['best_record'] = {'epoch': int(split_snapshot[1]),
                               'iter': int(split_snapshot[3]),
                               'val_loss': float(split_snapshot[5]),
                               'acc': float(split_snapshot[7]),
                               'acc_cls': float(split_snapshot[9]),
                               'mean_iu': float(split_snapshot[11]),
                               'fwavacc': float(split_snapshot[13])}

    net.cuda().train()

    mean_std = ([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

    train_joint_transform = joint_transforms.Compose([
        joint_transforms.Scale(args['width']),
        joint_transforms.RandomRotate(10),
        joint_transforms.RandomHorizontallyFlip()
    ])
    sliding_crop = joint_transforms.SlidingCrop(
        args['crop_size'], args['stride_rate'], ignore_label)
    train_input_transform = standard_transforms.Compose([
        standard_transforms.ToTensor(),
        standard_transforms.Normalize(*mean_std)
    ])
    val_input_transform = standard_transforms.Compose([
        standard_transforms.ToTensor(),
        standard_transforms.Normalize(*mean_std)
    ])
    target_transform = extended_transforms.MaskToTensor()
    visualize = standard_transforms.Compose([
        standard_transforms.Scale(args['val_img_display_size']),
        standard_transforms.ToTensor()
    ])

    geometrical_transform = None
    if args["affine"] is not None:
        view1, view2 = args["affine"]
        geometrical_transform = carla.GeometricalTransform(
            int(view1), int(view2), args["affine_type"], args["random"])

    train_set = carla.CarlaDataset('train',
                                   views=views,
                                   episodes=episodes,
                                   joint_transform=train_joint_transform,
                                   sliding_crop=sliding_crop,
                                   transform=train_input_transform,
                                   target_transform=target_transform,
                                   geometrical_transform=geometrical_transform,
                                   root=args['dataroot'])
    train_loader = DataLoader(train_set, batch_size=args[
                              'train_batch_size'], num_workers=8, shuffle=True)

    val_set = carla.CarlaDataset('val',
                                 views=views,
                                 episodes=episodes,
                                 transform=val_input_transform,
                                 sliding_crop=sliding_crop,
                                 target_transform=target_transform,
                                 geometrical_transform=geometrical_transform,
                                 root=args['dataroot'])
    val_loader = DataLoader(val_set, batch_size=1,
                            num_workers=8, shuffle=False)

    criterion = CrossEntropyLoss2d(
        size_average=True, ignore_index=ignore_label).cuda()

    optimizer = optim.SGD([
        {'params': [param for name, param in net.named_parameters() if name[-4:] == 'bias'],
         'lr': 2 * args['lr']},
        {'params': [param for name, param in net.named_parameters() if name[-4:] != 'bias'],
         'lr': args['lr'], 'weight_decay': args['weight_decay']}
    ], momentum=args['momentum'], nesterov=True)

    if len(args['snapshot']) > 0:
        optimizer.load_state_dict(torch.load(os.path.join(
            ckpt_path, exp_name, 'opt_' + args['snapshot'])))
        optimizer.param_groups[0]['lr'] = 2 * args['lr']
        optimizer.param_groups[1]['lr'] = args['lr']

    check_mkdir(ckpt_path)
    check_mkdir(os.path.join(ckpt_path, exp_name))
    open(os.path.join(ckpt_path, exp_name, str(
        datetime.datetime.now()) + '.txt'), 'w').write(str(args) + '\n\n')

    train_subrutine(train_loader, net, criterion, optimizer,
                    curr_epoch, args, val_loader, visualize, writer)


def train_subrutine(train_loader,
                    net,
                    criterion,
                    optimizer,
                    curr_epoch,
                    train_args,
                    val_loader,
                    visualize,
                    writer):
    while True:
        train_main_loss = AverageMeter()
        train_aux_loss = AverageMeter()
        curr_iter = (curr_epoch - 1) * len(train_loader)
        for i, data in enumerate(train_loader):
            optimizer.param_groups[0]['lr'] = 2 * train_args['lr'] * (1 - float(curr_iter) / train_args['max_iter']
                                                                      ) ** train_args['lr_decay']
            optimizer.param_groups[1]['lr'] = train_args['lr'] * (1 - float(curr_iter) / train_args['max_iter']
                                                                  ) ** train_args['lr_decay']

            inputs, gts, _ = data
            assert len(inputs.size()) == 5 and len(gts.size()) == 4
            inputs.transpose_(0, 1)
            gts.transpose_(0, 1)

            assert inputs.size()[3:] == gts.size()[2:]
            slice_batch_pixel_size = inputs.size(
                1) * inputs.size(3) * inputs.size(4)

            for inputs_slice, gts_slice in zip(inputs, gts):
                inputs_slice = Variable(inputs_slice).cuda()
                gts_slice = Variable(gts_slice).cuda()

                optimizer.zero_grad()
                outputs, aux = net(inputs_slice)
                assert outputs.size()[2:] == gts_slice.size()[1:]
                assert outputs.size()[1] == len(
                    carla.CarlaDataset.CLASSES.keys())

                main_loss = criterion(outputs, gts_slice)
                aux_loss = criterion(aux, gts_slice)
                loss = main_loss + 0.4 * aux_loss
                loss.backward()
                optimizer.step()

                train_main_loss.update(
                    main_loss.data[0], slice_batch_pixel_size)
                train_aux_loss.update(aux_loss.data[0], slice_batch_pixel_size)

            curr_iter += 1
            writer.add_scalar('train_main_loss',
                              train_main_loss.avg, curr_iter)
            writer.add_scalar('train_aux_loss', train_aux_loss.avg, curr_iter)
            writer.add_scalar('lr', optimizer.param_groups[1]['lr'], curr_iter)

            if (i + 1) % train_args['print_freq'] == 0:
                print('[epoch %d], [iter %d / %d], [train main loss %.5f], [train aux loss %.5f]. [lr %.10f]' % (
                    curr_epoch, i +
                    1, len(train_loader), train_main_loss.avg, train_aux_loss.avg,
                    optimizer.param_groups[1]['lr']))
            if curr_iter >= train_args['max_iter']:
                return
            if curr_iter % train_args['val_freq'] == 0:
                validate(val_loader, net, criterion, optimizer,
                         curr_epoch, i + 1, train_args, visualize, writer)
        curr_epoch += 1

# the following code is written assuming that batch size is 1
def validate(val_loader,
             net,
             criterion,
             optimizer,
             epoch,
             iter_num,
             train_args,
             visualize,
             writer,
             save=True):

    net.eval()
    ignore_label = train_args['ignore']

    val_loss = AverageMeter()

    h, w = train_args['height'], train_args['width']

    if train_args['affine_type'] == 'crop':
        view1, view2 = train_args["affine"]
        M, bbox, min_box = fh.load_mask_boxes(train_args['episodes'][0], int(view1), int(view2))  # FIXME just one episode
        _, _, w, h = bbox

    gts_all = np.zeros(
        (len(val_loader), h, w), dtype=int)
    predictions_all = np.zeros(
        (len(val_loader), h, w), dtype=int)

    filename = "/dataset/episode_0{}/camera_{}{}/image_{:0>5d}.png"

    for vi, data in enumerate(val_loader):
        input, gt, slices_info = data
        assert len(input.size()) == 5 and len(
            gt.size()) == 4 and len(slices_info.size()) == 3
        input.transpose_(0, 1)
        gt.transpose_(0, 1)
        slices_info.squeeze_(0)
        assert input.size()[3:] == gt.size()[2:]

        count = torch.zeros(h, w).cuda()
        output = torch.zeros(len(carla.CarlaDataset.CLASSES.keys()), h, w).cuda()

        slice_batch_pixel_size = input.size(1) * input.size(3) * input.size(4)

        for input_slice, gt_slice, info in zip(input, gt, slices_info):
            input_slice = Variable(input_slice).cuda()
            gt_slice = Variable(gt_slice).cuda()

            output_slice = net(input_slice)
            assert output_slice.size()[2:] == gt_slice.size()[1:]
            assert output_slice.size()[1] == len(
                carla.CarlaDataset.CLASSES.keys())
            output[:, info[0]: info[1], info[2]: info[3]
                   ] += output_slice[0, :, :info[4], :info[5]].data
            gts_all[vi, info[0]: info[1], info[2]: info[3]
                    ] += gt_slice[0, :info[4], :info[5]].data.cpu().numpy()

            count[info[0]: info[1], info[2]: info[3]] += 1

            val_loss.update(criterion(output_slice, gt_slice).data[
                            0], slice_batch_pixel_size)

        output /= count
        gts_all[vi, :, :] = gts_all[
            vi, :, :] // count.cpu().numpy().astype(int)
        predictions_all[vi, :, :] = output.max(0)[1].squeeze_(0).cpu().numpy()

        if train_args["mode"] == "video":
            predictions_pil = carla.colorize_mask(predictions_all[vi, :, :]).convert('RGB')
            fname = filename.format(train_args["episodes"][0], train_args["views"][0], train_args["postfix"], vi)
            print ("saving {}".format(fname))
            # cv2.imwrite(fname, cv2.cvtColor(np.array(vis), cv2.COLOR_RGBA2BGR))
            predictions_pil.save(fname)


        print('validating: %d / %d' % (vi + 1, len(val_loader)))

    acc, acc_cls, mean_iu, fwavacc, hist = evaluate(
        predictions_all, gts_all, len(carla.CarlaDataset.CLASSES.keys()), ignore=ignore_label)


    if train_args['mode'] == 'test':
        confusion_matrix_name = 'confusion_matrix_{}_{}.npy'.format(train_args["episodes"][0], train_args["views"][0])
        print("Saving {}".format(confusion_matrix_name))
        np.save(os.path.join(train_args['ckpt_path'], train_args['exp_name'], confusion_matrix_name), hist)


    if save:
        if val_loss.avg < train_args['best_record']['val_loss']:
            train_args['best_record']['val_loss'] = val_loss.avg
            train_args['best_record']['epoch'] = epoch
            train_args['best_record']['iter'] = iter_num
            train_args['best_record']['acc'] = acc
            train_args['best_record']['acc_cls'] = acc_cls
            train_args['best_record']['mean_iu'] = mean_iu
            train_args['best_record']['fwavacc'] = fwavacc
        snapshot_name = 'epoch_%d_iter_%d_loss_%.5f_acc_%.5f_acc-cls_%.5f_mean-iu_%.5f_fwavacc_%.5f_lr_%.10f' % (
            epoch, iter_num, val_loss.avg, acc, acc_cls, mean_iu, fwavacc, optimizer.param_groups[1]['lr'])
        torch.save(net.state_dict(), os.path.join(
            train_args['ckpt_path'], train_args['exp_name'], snapshot_name + '.pth'))
        torch.save(optimizer.state_dict(), os.path.join(
            train_args['ckpt_path'], train_args['exp_name'], 'opt_' + snapshot_name + '.pth'))

        if train_args['val_save_to_img_file']:
            to_save_dir = os.path.join(
                train_args['ckpt_path'], train_args['exp_name'], '%d_%d' % (epoch, iter_num))
            check_mkdir(to_save_dir)

        val_visual = []
        for idx, data in enumerate(zip(gts_all, predictions_all)):
            gt_pil = carla.colorize_mask(data[0])
            predictions_pil = carla.colorize_mask(data[1])
            if train_args['val_save_to_img_file']:
                predictions_pil.save(os.path.join(
                    to_save_dir, '%d_prediction.png' % idx))
                gt_pil.save(os.path.join(to_save_dir, '%d_gt.png' % idx))
            val_visual += [visualize(gt_pil.convert('RGB')),
                           visualize(predictions_pil.convert('RGB'))]

        val_visual = vutils.make_grid(val_visual, nrow=2, padding=5)

    """
    if train_args["mode"] == "video":
        # FIXME
        filename = "/dataset/episode_0{}/camera_{}{}/image_{:0>5d}.png" #.format(train_args["episodes"], train_args["views"], train_args["postfix"])

        for idx, data in enumerate(zip(gts_all, predictions_all)):
            gt_pil = carla.colorize_mask(data[0]).convert('RGB')
            predictions_pil = carla.colorize_mask(data[1]).convert('RGB')
            vis = np.concatenate((gt_pil, predictions_pil), axis=1)
            fname = filename.format(train_args["episodes"][0], train_args["views"][0], train_args["postfix"], idx)
            print (fname)
            cv2.imwrite(fname, cv2.cvtColor(np.array(vis), cv2.COLOR_RGBA2BGR))
    """
    print('------------------------------------------------------------------')
    print('[epoch %d], [iter %d], [val loss %.5f], [acc %.5f], [acc_cls %.5f], [mean_iu %.5f], [fwavacc %.5f]' % (
        epoch, iter_num, val_loss.avg, acc, acc_cls, mean_iu, fwavacc))

    print('best record: [val loss %.5f], [acc %.5f], [acc_cls %.5f], [mean_iu %.5f], [fwavacc %.5f], [epoch %d], '
          '[iter %d]' % (train_args['best_record']['val_loss'], train_args['best_record']['acc'],
                         train_args['best_record']['acc_cls'], train_args[
                             'best_record']['mean_iu'],
                         train_args['best_record']['fwavacc'], train_args[
                             'best_record']['epoch'],
                         train_args['best_record']['iter']))

    print('------------------------------------------------------------------')

    if writer:
        writer.add_image(snapshot_name, val_visual)
        writer.add_scalar('val_loss', val_loss.avg, epoch)
        writer.add_scalar('acc', acc, epoch)
        writer.add_scalar('acc_cls', acc_cls, epoch)
        writer.add_scalar('mean_iu', mean_iu, epoch)
        writer.add_scalar('fwavacc', fwavacc, epoch)

    net.train()
    return val_loss.avg


def test(args):

    episodes = args["episodes"]
    views = args["views"]
    exp_name = args["exp_name"]
    ckpt_path = args["ckpt_path"]

    ignore_label = 255
    # writer = SummaryWriter(os.path.join(ckpt_path, 'exp', exp_name))

    mean_std = ([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    if args["mean"] == "b":
        mean_std = ([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])

    test_input_transform = standard_transforms.Compose([
        standard_transforms.ToTensor(),
        standard_transforms.Normalize(*mean_std)
    ])

    visualize = standard_transforms.Compose([
        standard_transforms.Scale(args['val_img_display_size']),
        standard_transforms.ToTensor()
    ])

    sliding_crop = None
    if args["sliding"]:
        sliding_crop = joint_transforms.SlidingCrop(
            args['crop_size'], args['stride_rate'], ignore_label)

    target_transform = extended_transforms.MaskToTensor()

    geometrical_transform = None
    if args["affine"] is not None:
        view1, view2 = args["affine"]
        geometrical_transform = carla.GeometricalTransform(
            int(view1), int(view2), args["affine_type"], args["random"])

    test_set = carla.CarlaDataset(args['mode'],
                                  views=views,
                                  episodes=episodes,
                                  transform=test_input_transform,
                                  sliding_crop=sliding_crop,
                                  geometrical_transform=geometrical_transform,
                                  target_transform=target_transform,
                                  root=args['dataroot'])
    test_loader = DataLoader(test_set, batch_size=args[
        'test_batch_size'], num_workers=8, shuffle=(args["mode"] == 'test'))

    net = psp_net.PSPNet(num_classes=len(carla.CarlaDataset.CLASSES.keys()), batch=args["nobatchnorm"])
    if args['multi']:
        net = torch.nn.DataParallel(net)

    net.cuda().eval()

    net.load_state_dict(torch.load(os.path.join(
        ckpt_path, exp_name, args['snapshot'])))
    split_snapshot = args['snapshot'].split('_')
    try:
        curr_epoch = int(split_snapshot[1]) + 1
        args['best_record'] = {'epoch': int(split_snapshot[1]),
                               'iter': int(split_snapshot[3]),
                               'val_loss': float(split_snapshot[5]),
                               'acc': float(split_snapshot[7]),
                               'acc_cls': float(split_snapshot[9]),
                               'mean_iu': float(split_snapshot[11]),
                               'fwavacc': float(split_snapshot[13])}
    except:
        curr_epoch = 0
        args['best_record'] = {'epoch': 0,
                               'iter': 0,
                               'val_loss': 0,
                               'acc': 0,
                               'acc_cls': 0,
                               'mean_iu': 0,
                               'fwavacc': 0}

    criterion = CrossEntropyLoss2d(
        size_average=True, ignore_index=ignore_label).cuda()

    optimizer = optim.SGD([
        {'params': [param for name, param in net.named_parameters() if name[-4:] == 'bias'],
         'lr': 2 * args['lr']},
        {'params': [param for name, param in net.named_parameters() if name[-4:] != 'bias'],
         'lr': args['lr'], 'weight_decay': args['weight_decay']}
    ], momentum=args['momentum'], nesterov=True)

    try:
        optimizer.load_state_dict(torch.load(os.path.join(
            ckpt_path, exp_name, 'opt_' + args['snapshot'])))
        optimizer.param_groups[0]['lr'] = 2 * args['lr']
        optimizer.param_groups[1]['lr'] = args['lr']

    except:
        print ("no optimizer found")

    validate(test_loader, net, criterion, optimizer,
             curr_epoch, 1, args, visualize, writer=None, save=False)


if __name__ == '__main__':

    psp_options = PspOptions()
    psp_options.initialize()

    args = psp_options.parser.parse_args()

    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu # "0,1,2,3"

    if args.mode == 'train':
        train(vars(args))  # episodes=[17], views=[0], exp_name="Scene_1_1"

    elif args.mode == 'test' or args.mode == 'video':
        test(vars(args))

    else:
        train(vars(args))
        test(vars(args))
